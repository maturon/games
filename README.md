# Games

Random fun game-related projects. A work in progress.

## Current games

- Chess-CLI: a 2 player chess game written in python. All text-based currently.
- Sudoku-CLI: a sudoku puzzle generator written in python. All text-based currently.
