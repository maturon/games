import chess_logic

def parse_move(move, turn, board):
    # List to compare input against
    files   = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'}
    ranks   = {'1', '2', '3', '4', '5', '6', '7', '8'}
    pieces  = {'K', 'Q', 'R', 'B', 'N'}
    castles = {'o-o', 'O-O', 'o-o-o', 'O-O-O'}

    # Just in case input is wildly invalid, initializing will help safeguard some improper input
    source = None
    dest   = None
    move   = list(move)

    # While '+' is standard in chess notation, it is not necessary for logic verification
    if ('+' in move):                               # If a '+' is added to signify 'check'...
        move.remove('+')                            # Strip '+' in order to prevent len() from being thrown off later

    # All moves are between 2 and 5 characters long)
    if (len(move) > 5 or len(move) < 2):
        return source, dest

    # If input contains an 'x' then we know it's a capture move
    if ('x' in move):
        if (move[1] == 'x'):                        # If capture is at second position
            # Verify all remaining input is correct letters/numbers in correct order
            if (move[0] in files or move[0] in pieces):
                if (move[2] in files and move[3] in ranks):
                    source = move[0]                # Pawn capture, so source is simply file letter
                    dest   = move[2] + move[3]      # Destination is capture square
                    return source, dest
        elif (move[2] == 'x'):                      # If capture is at third position (i.e. 2 rooks/knights can capture on the same square)
            # Verify all remaining input is correct letters/numbers in correct order
            if (move[0] in pieces):
                if (move[1] in files or move[1] in ranks):
                    if (move[3] in files and move[4] in ranks):
                        source = move[0] + move[1]  # Source contains both piece letter AND rank/file letter/number to destinguish piece
                        dest   = move[3] + move[4]  # Destination is capture square
                        return source, dest

    # Pawn move logic
    elif (move[0] in files):                        # Pawn movement due to first letter being file a-h
        if (move[1] in ranks):                      # As long as second letter is rank number...
            source = None                           # Skip source value since notation dictates only destination needed for pawn moves
            dest   = move[0] + move[1]              # Destination is the move itself
            return source, dest

    # Piece move logic
    elif (move[0] in pieces):                       # Non-pawn piece move due to non-file letter being start of input
        if (len(move) == 3):                        # Use len() to determine if only first element in list is piece
            # Verify all remaining input is correct letters/numbers in correct order
            if (move[1] in files and move[2] in ranks):
                source = move[0]                    # First element is piece
                dest   = move[1] + move[2]          # Second and third element are destination
                return source, dest
        elif (len(move) == 4):                      # If len() is 1 more element long, second element will be to destinguish which piece
            # Verify all remaining input is correct letters/numbers in correct order
            if (move[1] in files or move[1] in ranks):
                if (move[2] in files and move[3] in ranks):
                    source = move[0] + move[1]      # So we include distinguishing element in source
                    dest   = move[2] + move[3]      # Destination is remaining 2 elements
                    return source, dest

    # Check if input is castle
    elif (''.join(map(str, move)) in castles):      # Convert move (list of elements) to str for comparison with castles list above
        source = 'K'
        dest = ''.join(map(str, move))
        return source, dest

    # If all else fails, send back null values
    return source, dest


# Move piece will call on additional functions to perform logical checks per piece
def move_piece(piece, dest, color, board, turn):

    # In some rare circumstances, two identical pieces may claim the same destination square...
    # So we specify a modifier (the second letter/number in our chess notation) to be more specific
    # Ex. Nexf5 - In this scenario, it is assumed that 2 different knights can both capture on f5 so the 'e' knight is chosen
    # Otherwise, the var piece is just a single char
    if (len(str(piece)) == 2):
        modifier = piece[1]
        piece    = piece[0]
    else:
        modifier = None

    # Pawn moves will either mean piece is null or just a file and no rank
    file = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'}

    if (piece == None or piece in file):
        if (chess_logic.verify_pawn(piece, color, dest, board, turn)):
            return True
    elif (piece == 'N'):
        if (chess_logic.verify_knight(color, dest, board, modifier)):
            return True
    elif (piece == 'B'):
        if (chess_logic.verify_bishop(color, dest, board, modifier)):
            return True
    elif (piece == 'R'):
        if (chess_logic.verify_rook(color, dest, board, modifier)):
            return True
    elif (piece == 'Q'):
        if (chess_logic.verify_queen(color, dest, board, modifier)):
            return True
    else:
        if (chess_logic.verify_king(color, dest, board)):
            return True
    return False

    dest = chessboard.get_coords(dest)
    if (chess_logic.square_attacked(dest, color, board)):
        print("Yup!")
    else: print ("narp")


def error(msg):
    print(msg)
    print()
    exit(1)
