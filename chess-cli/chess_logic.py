# The heart of the input for this program: parses standard chess notation input
# Breaks down user input to determine what piece to move, where to move it,
# whether or not to capture with it, whether or not the move is legal, etc.
import chess_move
import chessboard
import chess_pieces

def verify_pawn(file, color, dest, board, turn):
    # Convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately nullify move if trying to land on a square with same color occupant
    if (board[dest] != None and board[dest].color == color): return False

    # Flag for if we have any pieces that can legal make requested move
    verify_success = False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    # Promotion squares
    terminal_squares    = {0, 1, 2, 3, 4, 5, 6, 7, 56, 57, 58, 59, 60, 61, 62, 63}
    a_file_squares      = {0, 8, 16, 24, 32, 40, 48, 56}
    h_file_squares      = {7, 15, 23, 31, 39, 47, 55, 63}

    # Perform pawn promotion
    def promote_pawn(color):
        # Pawns cannot be promoted to a King or back to a pawn
        valid_pieces = {'Q', 'R', 'B', 'N', 'q', 'r', 'b', 'n'}
        while True:
            new_piece = str(input("Promote pawn to: "))
            if (str(new_piece) not in valid_pieces):
                print("Invalid piece promotion. Must be Q | R | B | N")
                continue
            break
        if (new_piece == 'Q' or new_piece == 'q'):
            new_piece = chess_pieces.Queen(color)
        elif (new_piece == 'R' or new_piece == 'r'):
            new_piece = chess_pieces.Rook(color)
        elif (new_piece == 'B' or new_piece == 'b'):
            new_piece = chess_pieces.Bishop(color)
        else:
            new_piece = chess_pieces.Knight(color)
        return new_piece

    if (color == 'White'):                                      # Determine color of piece being moved so that we can abstract variables
        single_move   = 8                                       # Move up (for white)
        double_move   = 16                                      # Double move up (for white)
        capture_left  = 7                                       # Capture up and left (for white)
        capture_right = 9                                       # Capture up and right (for white)
        double_rank   = {24, 25, 26, 27, 28, 29, 30, 31}        # Used to determine if white pawn move might be a double move
        ep_rank       = {40, 41, 42, 43, 44, 45, 46, 47}        # Dest after en passant capture (for white)
    else:                                                       # Black pawn logic below
        single_move   = -8                                      # Move down (as black)
        double_move   = -16                                     # Double move down (as black)
        capture_left  = -9                                      # Capture down and left (as black)
        capture_right = -7                                      # Capture down and right (as black)
        double_rank   = {32, 33, 34, 35, 36, 37, 38, 39}        # Used to determine if black pawn move might be a double move
        ep_rank       = {16, 17, 18, 19, 20, 21, 22, 23}        # Dest after en passant capture (for black)

    if (file == None):                                          # If there is no file parsed then we are simply moving a pawn to dest square
        if (board[dest] != None): return False                  # If dest square is occupied, move is invalid
        if (dest in double_rank):                               # If dest square is in 4th rank (as white) or 5th rank (as black)...
            if (board[dest - single_move] == None and           # If there's no piece blocking the move...
               (board[dest - double_move] != None and           # And a piece exists 2 previous squares from dest square...
               (board[dest - double_move].name == 'pawn' and    # And that piece is a pawn of the correct color that hasn't moved yet...
                board[dest - double_move].color == color and not
                board[dest - double_move].has_moved))):
                board[dest] = board[dest - double_move]     # Move pawn twice and flag as having moved to prevent double move in the future
                board[dest - double_move] = None
                if (king_safe((dest - double_move), dest, color, board, captured)):
                    board[dest].has_moved = True
                    board[dest].turn = turn
                    verify_success = True
                else:
                    print("king is in check or being put in check!")
                    return False
        if (board[dest - single_move] != None and               # Otherwise, if there's a correct color pawn on the previous square, move it
           (board[dest - single_move].name == 'pawn' and
            board[dest - single_move].color == color)):
            board[dest] = board[dest - single_move]
            board[dest - single_move] = None
            if (king_safe((dest - single_move), dest, color, board, captured)):
                board[dest].turn = turn
                verify_success = True
            else:
                print("king is in check or being put in check!")
                return False


    else:                                                       # If file has a value, then we perform pawn capture logic
        if (file == 'a'):                                       # If pawn is on 'a' file...
            if (dest in h_file_squares): return False           # Do not allow it to capture on the 'h' file
        if (file == 'h'):                                       # Likewise, if pawn is on 'h' file...
            if (dest in a_file_squares): return False           # Don't let it capture on 'a' file
        if (board[dest] != None and
            board[dest].color == color): return False           # Can't capture a piece of the same color

        # Get file of square down/left (for white) or up/left (for black) of dest
        left_source = chessboard.get_notation(dest - capture_right)
        left_source = left_source[0]

        # Get file of square down/right (for white) or up/right (for black) from dest
        right_source = chessboard.get_notation(dest - capture_left)
        right_source = right_source[0]

        if (board[dest] == None):                                       # Possible invalid move, but first, check for capture en passant
            if (board[dest - single_move] != None and                   # If square below (for white) or above (for black) dest is occupied...
                board[dest - single_move].color != color and            # By a pawn of the opposite color...
                board[dest - single_move].name == 'pawn' and
                board[dest - single_move].turn == (turn - 1)):          # And current turn is 1 ahead of the turn that pawn made a double move...
                if (board[dest - capture_right] != None and             # Determine which square contains a viable attacking pawn...
                    board[dest - capture_right].name == 'pawn' and
                    board[dest - capture_right].color == color and
                    left_source == file):
                    captured = board[dest]
                    board[dest] = board[dest - capture_right]           # If source is down/left (for white) or up/left (for black) from dest...
                    board[dest - capture_right] = None                  # Capture enemy pawn as though it only made a single move (not double)
                    board[dest - single_move] = None
                    if (king_safe((dest - capture_right), dest, color, board, captured)):
                        verify_success = True
                    else:
                        print("king is in check or being put in check!")
                        return False
                elif (board[dest - capture_left] != None and            # Otherwise, check other possible source of capture and perform same logic
                    board[dest - capture_left].name == 'pawn' and
                    board[dest - capture_left].color == color and
                    right_source == file):
                    captured = board[dest]
                    board[dest] = board[dest - capture_left]
                    board[dest - capture_left] = None
                    board[dest - single_move] = None
                    if (king_safe((dest - capture_left), dest, color, board, captured)):
                        verify_success = True
                    else:
                        print("king is in check or being put in check!")
                        return False
            else: return False

        if (board[dest - capture_right] != None and             # If source of capture is a pawn that matches our color and file...
            board[dest - capture_right].name == 'pawn' and
            board[dest - capture_right].color == color and
            left_source == file):
            captured = board[dest]
            board[dest] = board[dest - capture_right]           # Capture piece
            board[dest - capture_right] = None
            if (king_safe((dest - capture_right), dest, color, board, captured)):
                verify_success = True
            else:
                print("king is in check or being put in check!")
                return False
        elif (board[dest - capture_left] != None and            # Otherwise, check other possible source of capture and perform same logic
            board[dest - capture_left].name == 'pawn' and
            board[dest - capture_left].color == color and
            right_source == file):
            captured = board[dest]
            board[dest] = board[dest - capture_left]
            board[dest - capture_left] = None
            if (king_safe((dest - capture_left), dest, color, board, captured)):
                verify_success = True
            else:
                print("king is in check or being put in check!")
                return False
    if (verify_success):                                        # If we successfully moved our pawn, check for promotion below
        if (dest in terminal_squares):                          # Check to see if dest is a promotion square
            board[dest] = promote_pawn(color)                   # And promote pawn if it is
    return verify_success





def verify_knight(color, dest, board, modifier):
    # Convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately verify that a piece of the same color does not occupy our destination square
    if (board[dest] != None and board[dest].color == color): return False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    # Adding/subtracting from list below gives us knight movements from source square or to/from a destination square
    move_options = [6, 10, 15, 17]

    # Bounds checking will occur with the following files and their respective square coords
    file_a = [0, 8, 16, 24, 32, 40, 48, 56]
    file_b = [1, 9, 17, 25, 33, 41, 49, 57]
    file_g = [6, 14, 22, 30, 38, 46, 54, 62]
    file_h = [7, 15, 23, 31, 39, 47, 55, 63]

    dest_square_color = chessboard.get_square_color(dest)
    verify_success = False
    source = None

    # Init our potential source square as a list in case of multiple viable candidates
    candidates = []

    # List to hold all possible knights that can occupy the same dest square
    final_candidates = []

    # Eventually, if at least once candidate exists, it will be returned via source
    source = None

    for x in range(0, 64):
        # Candidate knights will be of the same color (obviously) and the dest square color and knight square color will be opposite
        if (board[x] != None and
            board[x].name == 'knight' and
            board[x].color == color and
            chessboard.get_square_color(x) != dest_square_color):
            candidates.append(x)

    # Iterate through all knights of the correct color that are on square color opposite our dest square (knights jump to opposite color)
    for candidate in candidates:

        viable_squares = []     # Initial squares a given candidate can move
        remove_squares = []     # Squares that are outside the bounds of a candidate's movement
        final_squares  = []     # Finalized list of viable moves after out-of-bounds moves have been removed

        # Iterate through maximum possible moves per candidate
        for move in move_options:

            # Build our viable_squares list of values within our 0-63 square range
            if (candidate + move < 64): viable_squares.append(candidate + move)
            if (candidate - move >= 0): viable_squares.append(candidate - move)


        # Iterate through those viable_squares and build a remove_squares list for all out-of-bounds squares (before 'a' and after 'h')
        for square in range(len(viable_squares)):
            if (candidate in file_a):
                if (viable_squares[square] == (candidate + 15) or
                    viable_squares[square] == (candidate + 6) or
                    viable_squares[square] == (candidate - 10) or
                    viable_squares[square] == (candidate - 17)):
                    remove_squares.append(viable_squares[square])
            elif (candidate in file_b):
                if (viable_squares[square] == (candidate + 6) or
                    viable_squares[square] == (candidate - 10)):
                    remove_squares.append(viable_squares[square])
            elif (candidate in file_g):
                if (viable_squares[square] == (candidate + 10) or
                    viable_squares[square] == (candidate - 6)):
                    remove_squares.append(viable_squares[square])
            elif (candidate in file_h):
                if (viable_squares[square] == (candidate + 17) or
                    viable_squares[square] == (candidate + 10) or
                    viable_squares[square] == (candidate - 6) or
                    viable_squares[square] == (candidate - 15)):
                    remove_squares.append(viable_squares[square])

        # Iterate through viable_squares and build a final_squares list by skipping any of the remove_squares elements
        for y in range(len(viable_squares)):
            if (viable_squares[y] in remove_squares):
                continue
            final_squares.append(viable_squares[y])

        # Compare the final_squares list for this candidate to dest square to see if we have a match
        for z in final_squares:
            if (z == dest):
                final_candidates.append(candidate)
                verify_success = True

    # If we end up with no viable piece to move to the dest square, exit
    if (not final_candidates): return False

    # If we have multiple candidate pieces, use modifier to determine preffered piece to move
    elif (len(final_candidates) > 1):
        source = get_correct_candidate(final_candidates, dest, modifier)
    else:
        source = int(final_candidates[0])

    # As long as get_correct_candidate() returned a value or there is only one candidate, move our piece
    if (source == None): return False
    if (verify_success):
        if (board[dest] != None): captured  = board[dest]
        board[dest] = board[source]
        board[source] = None
        if (king_safe(source, dest, color, board, captured)):
            return verify_success
        else:
            print("king is in check or being put in check!")
            return False



def verify_bishop(color, dest, board, modifier):
    # Convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately verify that a piece of the same color does not occupy our destination square
    if (board[dest] != None and board[dest].color == color): return False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    source = None
    source = verify_diags('bishop', dest, color, board, modifier)

    # Invalid move if player tries to 'skip' move by requesting a dest square already containing one of his bishops
    if (source == dest): return False
    if (source == None): return False

    if (board[dest] != None): captured = board[dest]
    board[dest] = board[source]
    board[source] = None
    if (king_safe(source, dest, color, board, captured)):
        return True
    else:
        print("king is in check or being put in check!")
        return False




def verify_rook(color, dest, board, modifier):
    # Convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately verify that a piece of the same color does not occupy our destination square
    if (board[dest] != None and board[dest].color == color): return False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    # Init a candidates list in case we have multiple viable pieces to move to our dest square
    candidates = []
    source = None

    # Obtain candidates from rank and file of dest square
    rank = verify_rank('rook', dest, color, board, modifier)
    file = verify_file('rook', dest, color, board, modifier)

    if (rank != None):                                              # If we have a candidate on the same rank...
        candidates.append(rank)                                     # Add it to our list of candidates
    if (file != None):                                              # Same with candidates on same file as dest square
        candidates.append(file)
    if (len(candidates) == 0): return False                         # If for some reason we still don't have a candidate piece, exit

    elif (len(candidates) == 1):
        source = candidates[0]
    elif (len(candidates) > 1):                                       # If we end up with multiple candidates...
        source = get_correct_candidate(candidates, dest, modifier)  # Use modifier to determine correct candidate

    if (source != None):                                            # As long as we have a source...
        if (board[dest] != None): captured = board[dest]
        board[dest] = board[source]                                 # Place candidate on dest square
        board[source] = None                                        # Remove candidate from source square
        if (king_safe(source, dest, color, board, captured)):
            if (not board[dest].has_moved):                         # If that source has not moved...
                board[dest].has_moved = True                        # Flag as having moved (useful in determining if a king can castle)
            return True
        else:
            print("king is in check or being put in check!")
            return False
    else: return False




def verify_queen(color, dest, board, modifier):
    # Convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately verify that a piece of the same color does not occupy our destination square
    if (board[dest] != None and board[dest].color == color): return False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    # Init a candidates list in case we have multiple viable pieces to move to our dest square
    candidates = []
    source = None

    # Obtain potential candidates for all ranks, files, and diagonals coming from dest square
    rank = verify_rank('queen', dest, color, board, modifier)
    file = verify_file('queen', dest, color, board, modifier)
    diag = verify_diags('queen', dest, color, board, modifier)

    if (rank != None):                          # If we have a queen on the same rank, add to list of candidates
        candidates.append(rank)
    if (file != None):                          # Same for queens on same file...
        candidates.append(file)
    if (diag != None):                          # And queens on diagonals connect to dest square
        candidates.append(diag)

    if (len(candidates) == 0): return False     # If there are no viable queens, exit
    elif (len(candidates) == 1):                # Exactly one viable queen, return it as our piece to move
        source = candidates[0]
    elif (len(candidates) > 1):                 # More than one queen, figure out correct queen based on modifier passed
        source = get_correct_candidate(candidates, dest, modifier)

    if (source != None):                        # As long as we have a source square to move from...
        if (board[dest] != None): captured = board[dest]
        board[dest] = board[source]             # Move to our dest square
        board[source] = None                    # And remove from our source square
        if (king_safe(source, dest, color, board, captured)):
            return True
        else:
            print("king is in check or being put in check!")
            return False
    else: return False




def verify_king(color, dest, board):
    if (dest == 'O-O' or dest == 'o-o'):        # If kingside castle...
        if (color == 'White'):                  # If white is castling...
            if (board[5] == None and            # Make sure no pieces occupy squares between king and rook
                board[6] == None and not
                board[4].has_moved and not      # Verify king has not moved yet...
                board[7].has_moved):
                if (not square_attacked(4, color, board) and not
                square_attacked(5, color, board) and not
                square_attacked(6, color, board)):
                    board[6] = board[4]             # Perform castle (move king and rook) and flag both pieces to prevent future castling
                    board[4] = None
                    board[6].has_moved = True
                    board[5] = board[7]
                    board[7] = None
                    board[5].has_moved = True
                    return True
                else:
                    print("King cannot castle out of, through, or into check!")
            return False
        else:                                   # Same for black, just different squares in question
            if (board[61] == None and
                board[62] == None and not
                board[60].has_moved and not
                board[63].has_moved):
                if (not square_attacked(60, color, board) and not
                square_attacked(61, color, board) and not
                square_attacked(62, color, board)):
                    board[62] = board[60]           # Perform castle (move king and rook) and flag both pieces to prevent future castling
                    board[60] = None
                    board[62].has_moved = True
                    board[61] = board[63]
                    board[63] = None
                    board[61].has_moved = True
                    return True
                else:
                    print("King cannot castle out of, through, or into check!")
            return False

    elif (dest == 'O-O-O' or dest == 'o-o-o'):  # If queenside castle...
        if (color == 'White'):                  # If white is castling...
            if (board[1] == None and            # Make sure no pieces occupy squares between king and rook (1 more square to check for queenside)
                board[2] == None and
                board[3] == None and not
                board[4].has_moved and not      # Verify king has not moved yet...
                board[0].has_moved):            # And rook has not moved yet...
                if (not square_attacked(4, color, board) and not
                square_attacked(3, color, board) and not
                square_attacked(2, color, board)):
                    board[2] = board[4]             # Perform castle (move king and rook) and flag both pieces to prevent future castling
                    board[4] = None
                    board[2].has_moved = True
                    board[3] = board[0]
                    board[0] = None
                    board[3].has_moved = True
                    return True
                else:
                    print("King cannot castle out of, through, or into check!")
            return False
        else:                                   # Same for black, just different squares in question
            if (board[57] == None and
                board[58] == None and
                board[59] == None and not
                board[60].has_moved and not
                board[56].has_moved):
                if (not square_attacked(60, color, board) and not
                square_attacked(59, color, board) and not
                square_attacked(58, color, board)):
                    board[58] = board[60]           # Perform castle (move king and rook) and flag both pieces to prevent future castling
                    board[60] = None
                    board[58].has_moved = True
                    board[59] = board[56]
                    board[56] = None
                    board[59].has_moved = True
                    return True
                else:
                    print("King cannot castle out of, through, or into check!")
            return False

    # If our move is not a castle (see above), convert our chess notation to a grid number for easier mathematical calculations
    dest = chessboard.get_coords(dest)

    # Immediately verify that a piece of the same color does not occupy our destination square
    if (board[dest] != None and board[dest].color == color): return False

    # If a piece gets captured, store in case the move must be taken back
    captured = None

    # Boundaries
    rank_1 = {0, 1, 2, 3, 4, 5, 6, 7}
    rank_8 = {56, 57, 58, 59, 60, 61, 62, 63}
    file_a = {0, 8, 16, 24, 32, 40, 48, 56}
    file_h = {7, 15, 23, 31, 39, 47, 55, 63}

    # Maximum number of moves a King can move given no bounds
    moves = [-9, -8, -7, -1, 1, 7, 8, 9]

    # Init list that will be used to remove any out-of-bounds squares
    remove = []

    # Build our removal list by bounds checking our destination square
    if (at_rank_8(dest)): remove.extend((7, 8, 9))
    if (at_file_h(dest)): remove.extend((-7, 1, 9))
    if (at_file_a(dest)): remove.extend((-9, -1, 7))
    if (at_rank_1(dest)): remove.extend((-9, -8, -7))

    # Remove all elements from remove list from our moves list giving us current possible moves to destination square
    moves = [x for x in moves if x not in remove]

    # Iterate through moves list
    for x in moves:
        # If current move + dest square is found within move list and the square has a king of the correct color on it, move our king
        square = x + dest
        if (board[square] != None and board[square].name  == 'king' and board[square].color == color):
            if (board[dest] != None): captured = board[dest]
            board[dest] = board[square]
            board[square] = None
            if (king_safe(square, dest, color, board, captured)):
                # If the king has not moved yet, flag as having moved (to prevent castling in the future)
                if (not board[dest].has_moved):
                    board[dest].has_moved = True
                return True
            else:
                print("king is in check or being put in check!")
                return False
    return False




def verify_rank(piece, dest, color, board, modifier):

    candidates = []

    for x in range(dest, 64, 1):            # Iterate through the board by 1 (should soon be stopped by bounds check)
        if (at_file_h(x)):    # Bounds to check to make sure we don't go beyond 'h' file
            if (x == dest): break           # If this is our first iteration through and we're on the boundary, no need to continue this loop
            elif (board[x] != None and      # Otherwise...
                board[x].name == piece and  # If piece is the one we're looking for...
                board[x].color == color):   # And it's the correct color (ours)...
                candidates.append(x)        # Add to our candidates list
                break
            else: break                     # Otherwise, we found nothing on the boundary, break from the loop
        elif (x == dest): continue          # Only checking squares coming from dest square, not dest square itself
        elif (board[x] == None): continue   # If nothing occupying square, continue iteration
        elif (board[x].name == piece and    # If we find our matching piece...
            board[x].color == color):       # And matching color...
            candidates.append(x)            # Add it to the list
            break
        else: break

    for x in range(dest, -1, -1):           # Perform same loop as above but instead of iterating right, we iterate left towards the 'a' file
        if (at_file_a(x)):     # Bounds to check to make sure we don't go beyond 'a' file
            if (x == dest): break
            elif (board[x] != None and
                board[x].name == piece and
                board[x].color == color):
                candidates.append(x)
                break
            else: break
        elif (x == dest): continue
        elif (board[x] == None): continue
        elif (board[x].name == piece and
            board[x].color == color):
            candidates.append(x)
            break
        else: break

    # If multiple candidates, use modifier to determine correct candidate
    if (len(candidates) > 1):
        candidates[0] = get_correct_candidate(candidates, dest, modifier)
    elif (len(candidates) == 0): return None

    # Otherwise, return our only candidate
    return candidates[0]



def verify_file(piece, dest, color, board, modifier):

    candidates = []

    for x in range(dest, 64, 8):            # Iterate through the board by 8 (should soon be stopped by bounds check)
        if (at_rank_8(x)):       # Bounds to check to make sure we don't go beyond 8th rank
            if (x == dest): break           # If this is our first iteration through and we're on the boundary, no need to continue this loop
            elif (board[x] != None and      # Otherwise...
                board[x].name == piece and  # If piece is the one we're looking for...
                board[x].color == color):   # And it's the correct color (ours)...
                candidates.append(x)        # Add to our candidates list
                break
            else: break                     # Otherwise, we found nothing on the boundary, break from the loop
        elif (x == dest): continue          # Only checking squares coming from dest square, not dest square itself
        elif (board[x] == None): continue   # If nothing occupying square, continue iteration
        elif (board[x].name == piece and    # If we find our matching piece...
            board[x].color == color):       # And matching color...
            candidates.append(x)            # Add it to the list
            break
        else: break

    for x in range(dest, -1, -8):           # Perform same loop as above but instead of iterating upward, we iterate downward towards bottom rank
        if (at_rank_1(x)):     # Bounds to check to make sure we don't go beyond 1st rank
            if (x == dest): break
            elif (board[x] != None and
                board[x].name == piece and
                board[x].color == color):
                candidates.append(x)
                break
            else: break
        elif (x == dest): continue
        elif (board[x] == None): continue
        elif (board[x].name == piece and
            board[x].color == color):
            candidates.append(x)
            break
        else: break

    # If multiple candidates, use modifier to determine correct candidate
    if (len(candidates) > 1):
        candidates[0] = get_correct_candidate(candidates, dest, modifier)
    elif (len(candidates) == 0): return None

    # Otherwise, return our only candidate
    return candidates[0]




# Verify piece to move diagonally by iterating through 4 seperate loops, each extending a different direction out from dest square
def verify_diags(piece, dest, color, board, modifier):
    # Init list of potential candidates for dest square
    candidates = []

    # Iterate through diagonal squares from dest square up and right
    for x in range(dest, 64, 9):
        if (at_rank_8(x) or      # If we reach the top of the board...
            at_file_h(x)):    # Or the right of the board...
            if (x == dest): break           # If this is the first iteration through and we're at our dest square, exit loop
            elif (board[x] != None and      # Otherwise, if there's a piece on the square...
                board[x].name == piece and  # And it's the piece we're looking to move...
                board[x].color == color):   # And it's our color...
                candidates.append(x)        # Add it to candidate list
                break
            else:
                break                       # Otherwise, we're at the boundary, so exit loop
        elif (x == dest): continue          # If we're not at the boundary but it's first iteration through and we're at dest, exit loop
        elif (board[x] == None): continue   # If nothing occupies square, keep iterating
        elif (board[x].name == piece and    # Otherwise, if something occupies the square and it's the piece we're looking to move...
              board[x].color == color):     # And it's our color...
              candidates.append(x)          # Add it to candidate list
              break
        else: break                         # Otherwise, exit loop

    # Iterate through diagonal squares from dest square to up and left, same as loop above but a different diagonal/direction
    for x in range(dest, 64, 7):
        if (at_rank_8(x) or
            at_file_a(x)):
            if (x == dest): break
            elif (board[x] != None and
                board[x].name == piece and
                board[x].color == color):
                candidates.append(x)
                break
            else:
                break
        elif (x == dest): continue
        elif (board[x] == None): continue
        elif (board[x].name == piece and
              board[x].color == color):
              candidates.append(x)
              break
        else: break

    # Iterate through diagonal squares from dest square to down and right, same as loop above but a different diagonal/direction
    for x in range(dest, -1, -7):
        if (at_rank_1(x) or
            at_file_h(x)):
            if (x == dest): break
            elif (board[x] != None and
                board[x].name == piece and
                board[x].color == color):
                candidates.append(x)
                break
            else:
                break
        elif (x == dest): continue
        elif (board[x] == None): continue
        elif (board[x].name == piece and
              board[x].color == color):
              candidates.append(x)
              break
        else: break

    # Iterate through diagonal squares from dest square to down and left, same as loop above but a different diagonal/direction
    for x in range(dest, -1, -9):
        if (at_rank_1(x) or
            at_file_a(x)):
            if (x == dest): break
            elif (board[x] != None and
                board[x].name == piece and
                board[x].color == color):
                candidates.append(x)
                break
            else:
                break
        elif (x == dest): continue
        elif (board[x] == None): continue
        elif (board[x].name == piece and
              board[x].color == color):
              candidates.append(x)
              break
        else: break

    # Once we have a list of candidates, if the list is empty, exit func
    if (len(candidates) == 0): return None

    # If only one candidate, return it
    elif (len(candidates) == 1): return candidates[0]

    # If multiple candidates, use get_correct_candidate() to determine correct piece via modifier arg passed
    elif (len(candidates) > 1):
        candidates[0] = get_correct_candidate(candidates, dest, modifier)

    return candidates[0]


# Boundary check funcs. Next 4 funcs are used to determine if a piece or square is on a boundary
def at_rank_1(square):
    boundary = {0, 1, 2, 3, 4, 5, 6, 7}
    if (square in boundary): return True
    return False



def at_rank_8(square):
    boundary = {56, 57, 58, 59, 60, 61, 62, 63}
    if (square in boundary): return True
    return False



def at_file_a(square):
    boundary = {0, 8, 16, 24, 32, 40, 48, 56}
    if (square in boundary): return True
    return False



def at_file_h(square):
    boundary = {7, 15, 23, 31, 39, 47, 55, 63}
    if (square in boundary): return True
    return False



# Used to verify correct piece to move when multiple pieces can reach a given destination square
def get_correct_candidate(candidates, dest, modifier):
    source = None
    if (modifier == None): return None          # If there's no modifier, we won't be able to determine which piece is supposed to move
    likely_candidate = []                       # Var used to determine if multiple pieces are still viable, even with a modifier
    for x in candidates:                        # Iterate through our candidate pieces
        square = chessboard.get_notation(x)     # Obtain square info for each piece to compare against our modifier
        if (modifier == square[0]):             # If modifier matches our piece's file...
            likely_candidate.append(x)          # flag our potential match and keep iterating
        if (modifier == str(square[1])):        # If modifier matches our piece's rank...
            likely_candidate.append(x)          # Flag for likely candidate
    if (len(likely_candidate) > 1):
        source = None
        return source
    else:
        source = likely_candidate[0]
        return source                           # Return our candidate piece's source square




# This func performs several operations to make sure the square given as an argument is not under attack by any enemy pieces
# It first checks all diagonals stemming from square for enemy queens/bishops/pawns that attack square
# Next it checks files and rows for enemy rooks/queens that attack square
# Finally it determines if any enemy knights attack square
# Returns false if none of the above criteria are met and square is 'safe'
def square_attacked(square, color, board):
    # Build lists of static variables for boundary checking
    rank_1 = {0, 1, 2, 3, 4, 5, 6, 7}
    rank_8 = {56, 57, 58, 59, 60, 61, 62, 63}
    file_a = {0, 8, 16, 24, 32, 40, 48, 56}
    file_b = {1, 9, 17, 25, 37, 41, 49, 57}
    file_g = {6, 14, 22, 30, 38, 46, 54, 62}
    file_h = {7, 15, 23, 31, 39, 47, 55, 63}

    # Create abstracted diagonal check and plug in end_square, iterator, and boundaries to format direction of diagonal check
    def diag_attacker(end_square, iterator, boundary1, boundary2):
        for x in range(square, end_square, iterator):
            if (x == square):                                   # If we're at our start point (and not on a boundary), skip
                if (x in boundary1 or x in boundary2):          # Otherwise, break out since nothing is beyond boundary
                    break
                else: continue
            if (board[x] != None):                              # If a piece exists on the diagonal...
                if (x == square + iterator):                    # First check if this is the very first square after initial square...
                    if (board[x].color != color and             # If it's a pawn of the opposite color, we're under attack
                        board[x].name == 'pawn'):
                        return True
                if (board[x].color != color and                 # Otherwise, check to see if an opposite color bishop/queen exists
                    (board[x].name == 'bishop' or
                    board[x].name == 'queen')):
                    return True                                 # If opposite color queen/bishop exists, we're under attack
                else: break
            if (x in boundary1 or x in boundary2):              # Otherwise, if we hit boundary, break out of this loop
                break
            else: continue                                      # Catch-all: square is empty, iterate through again

    # Create abstracted file/rank check and plug in end_square, iterator, and boundaries to format direction of rank/file
    def rank_file_attacker(end_square, iterator, boundary):
        for x in range(square, end_square, iterator):
            if (x == square):                                   # If we're checking the very first square...
                if (x in boundary):                             # If we start our loop at the boundary (going toward boundary), then break out
                    break
                else: continue                                  # Otherwise, we continue to next square in rank/file
            if (board[x] != None):                              # If a piece occupies the square we're checking...
                if (board[x].color != color and                 # And it's an opposite color rook/queen...
                    (board[x].name == 'rook' or
                    board[x].name == 'queen')):
                    return True                                 # We're under attack, exit func
                else: break                                     # If it's any other piece, it's blocking attackers, leave loop
            if (x in boundary): break                           # Once we hit the boundary, exit
            else: continue                                      # Catch-all: square is empty, iterate through again

    if (diag_attacker(64, 9, rank_8, file_h)): return True      # Check up/right diagonal
    if (diag_attacker(64, 7, rank_8, file_a)): return True      # Check up/left diagonal
    if (diag_attacker(-1, -7, rank_1, file_h)): return True     # Check down/right diagonal
    if (diag_attacker(-1, -9, rank_1, file_a)): return True     # Check down/left diagonal
    if (rank_file_attacker(64, 1, file_h)): return True         # Check from square to file 'h'
    if (rank_file_attacker(-1, -1, file_a)): return True        # Check from square to file 'a'
    if (rank_file_attacker(64, 8, rank_8)): return True         # Check from square to rank 8
    if (rank_file_attacker(-1, -8, rank_1)): return True        # Check from square to rank 1

    # If no ranks/files/diagonals have attackers, check for enemy knights
    move_options = [-17, -15, -10, -6, 6, 10, 15, 17]
    remove_a = [-17, -10, 6, 15]
    remove_b = [-10, 6]
    remove_g = [-6, 10]
    remove_h = [-15, -6, 10, 17]


    # Remove unnecessary attack squares from knights on a/b/g/h files
    if (square in file_a):
        for x in remove_a:
            if (x in move_options): move_options.remove(x)
    if (square in file_b):
        for x in remove_b:
            if (x in move_options): move_options.remove(x)
    if (square in file_g):
        for x in remove_g:
            if (x in move_options): move_options.remove(x)
    if (square in file_h):
        for x in remove_h:
            if (x in move_options): move_options.remove(x)

    # Iterate through all 'knight moves' from square to see if an enemy knight attacks it
    for x in move_options:
        test_square = x + square

        # Skip verifying squares that are above/below our board
        if (test_square < 0 or test_square > 63):
            continue
        if (board[test_square] != None and
            board[test_square].color != color and
            board[test_square].name == 'knight'):
            return True
    return False




def king_safe(source, dest, color, board, captured):
    # Iterate through board and find king
    for x in range(0, 63):
        if (board[x] != None and
            board[x].name == 'king' and
            board[x].color == color):
            king = x

    if (square_attacked(king, color, board)):
        board[source] = board[dest]
        if (captured != None):
            board[dest] = captured
        else:
            board[dest] = None
        return False
    return True
