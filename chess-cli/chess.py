#!/usr/bin/python

import chessboard
import chess_move
import sys
import re


def main():

    # List holding our chess piece objects, i.e. our 'chessboard'
    board = []

    # Initialize our chessboard with piece objects
    chessboard.init_squares(board)

    current_move = ''   # Player input
    total_moves  = []   # List of moves made by both players
    color        = ''   # Color to move (used in move logic)
    turn         = 1    # Var to help determine input, formating, etc.

    # If args are passed via command line:
    if (len(sys.argv) > 1):
        parsed_move = []
        counter = 0

        for x in range(1, len(sys.argv)):
            if (turn % 2 != 0):
                color = 'White'
            else:
                color = 'Black'
            current_move = sys.argv[x]
            parsed_move = chess_move.parse_move(current_move, color, board)
            if (parsed_move[1] is None ):
                print()
                print("Usage: ./run.py [move1] [move2] [move3] ...")
                print()
                print("Ex.    ./run.py d4 d5 c4 e6 cxd5 ...")
                print("or     ./run.py")
                print()
                exit(1)
            if (not chess_move.move_piece(parsed_move[0], parsed_move[1], color, board, turn)):
                print("An invalid move was detected. Exiting...")
                exit(1)
            turn += 1
            total_moves.append(current_move)

    chessboard.print_ascii_board(board)

    # Main game loop
    while True:
        if (turn % 2 != 0):
            color = 'White'
        else:
            color = 'Black'

        # Get move as input
        current_move = str(input(color + " to move: "))

        # parsed_move is list since parse_move() returns the piece to move and destination square
        parsed_move = []
        parsed_move = chess_move.parse_move(current_move, color, board)

        # First we test user input to make sure it is proper chess notation
        # parse_move() will return a null destination (2nd element in list) if user input is invalid
        if (parsed_move[1] == None):
            print("Invalid move, please try again.")
            continue

        # If the notation is correct, we verify that the intended move is actually valid
        # Modifications made to the board due to valid moves are made within the respective piece logic called by move_piece() below
        if (not chess_move.move_piece(parsed_move[0], parsed_move[1], color, board, turn)):
            print("Invalid move, please try again.")
            continue
        else:
            turn += 1
            # Update list of moves and print them
            total_moves.append(current_move)
            chessboard.print_game(total_moves)
            print()
            chessboard.print_ascii_board(board)

main()
