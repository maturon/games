class piece_colors:
    WHITE = '\033[0m'
    BLACK = '\033[95m'

class Pawn():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'P'
        self.print_symbol = ''
        self.name         = 'pawn'
        self.has_moved    = False
        self.turn         = ''
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'P')
        else:
            self.print_symbol = (piece_colors.BLACK + 'P')


class Knight():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'N'
        self.print_symbol = ''
        self.name         = 'knight'
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'N')
        else:
            self.print_symbol = (piece_colors.BLACK + 'N')


class Bishop():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'B'
        self.print_symbol = ''
        self.name         = 'bishop'
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'B')
        else:
            self.print_symbol = (piece_colors.BLACK + 'B')


class Rook():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'R'
        self.print_symbol = ''
        self.name         = 'rook'
        self.has_moved    = False
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'R')
        else:
            self.print_symbol = (piece_colors.BLACK + 'R')


class Queen():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'Q'
        self.print_symbol = ''
        self.name         = 'queen'
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'Q')
        else:
            self.print_symbol = (piece_colors.BLACK + 'Q')


class King():
    def __init__(self, color):
        self.color        = color
        self.symbol       = 'K'
        self.print_symbol = ''
        self.name         = 'king'
        self.has_moved    = False
        self.in_check     = False
        if (self.color == 'White'):
            self.print_symbol = (piece_colors.WHITE + 'K')
        else:
            self.print_symbol = (piece_colors.BLACK + 'K')
