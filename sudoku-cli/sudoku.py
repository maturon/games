#!/usr/bin/env python

############################################################################################
# Sudoku puzzle generator!
# Square generation (large and small) go from left to right, top to bottom,
# and are numbered accordingly.
#
# Ex.
#    Overall Sudoku grid         Box 0 (upper left quad) with boxes labeled
#        ___ ___ ___                          ___ ___ ___
#       | 0 | 1 | 2 |                        |0,0|0,1|0,2|
#       |---|---|---|                        |---|---|---|
#       | 3 | 4 | 5 |                        |0,3|0,4|0,5|
#       |---|---|---|                        |---|---|---|
#       | 6 | 7 | 8 |                        |0,6|0,7|0,8|
#       -------------                        -------------
# Author:
#   Maturon Miner III
#   maturon@gmail.com
#   Copyleft Feb. 2021
############################################################################################
from random import randint
from copy import deepcopy


class SudokuBox():
    def __init__(self, num):
        # Number corresponding to where the box is located on the puzzle grid
        self.num = num

        # Individual, or "small" squares
        self.squares  = []



def main():
    gen_puzzle(get_difficulty())



def get_difficulty():
    print()
    print("Welcome to the wonderful world of Sudoku!")
    print()
    print("Please choose a difficulty setting:")
    print("1) Easy")
    print("2) Moderate")
    print("3) Hard")
    print()

    # Get difficulty setting
    while True:
        diff_rating = input()
        try:
            diff_rating = int(diff_rating)
        except:
            print()
            print("No letters! Please try again:")
            print()
            continue
        if (diff_rating > 3 or diff_rating < 1):
            print()
            print("Sorry, that is not an option. Please try again")
            print()
            continue
        break
    return diff_rating




def gen_puzzle(diff_rating):

    # List of box objects (9 boxes to each be populated with 9 squares)
    completed_boxes = []
    modified_boxes = []

    # Generate seed box (upper left quad) with randomized numbers and append
    # to list of box objects
    seed = SudokuBox(0)
    completed_boxes.append(seed)
    seed_box(seed)

    # Generate all box objects needed for puzzle and populate with eligible values
    for x in range(1,9):
        completed_boxes.append(SudokuBox(x))
    gen_boxes(completed_boxes)

    modified_boxes = difficulty_mask(completed_boxes, diff_rating)

    print_box_row(modified_boxes[0], modified_boxes[1], modified_boxes[2])
    print_box_row(modified_boxes[3], modified_boxes[4], modified_boxes[5])
    print_box_row(modified_boxes[6], modified_boxes[7], modified_boxes[8])

    print()
    print("Press any key to see answer...")
    input()

    print_box_row(completed_boxes[0], completed_boxes[1], completed_boxes[2])
    print_box_row(completed_boxes[3], completed_boxes[4], completed_boxes[5])
    print_box_row(completed_boxes[6], completed_boxes[7], completed_boxes[8])



# Called to seed initial box of squares (upper left quad)
def seed_box(box):
    while True:
        num = randint(1,9)

        # Iterate through used numbers until an unused number is generated, then
        # store in corresponding square
        if (num in box.squares):
            continue
        else:
            box.squares.append(num)
            if (len(box.squares) == 9):
                break



def gen_boxes(boxes):
    # List for comparing random number candidates
    blacklist = []
    box_num = 1

    # Continue to generate eligible candidate squares and append to box until there are 9 elements in box
    while True:
        square = len(boxes[box_num].squares)
        blacklist.clear()

        # Build list of ineligible numbers and remove duplicates
        blacklist_rows(boxes[box_num], boxes, square, blacklist)
        blacklist_cols(boxes[box_num], boxes, square, blacklist)
        blacklist = list(set(blacklist))

        # RESET
        # Test random number against blacklist and all other numbers in current box
        # If we've painted ourselves into a corner (impossible logic), then
        # scrap this box and previous box and start over
        if (not gen_num(boxes[box_num], boxes, blacklist)):
            boxes[box_num].squares.clear()
            boxes[box_num - 1].squares.clear()
            box_num -= 1
            continue

        if (len(boxes[box_num].squares) == 9):
            box_num += 1
            if (box_num > 8):
                break


######################################################
# Build blacklist to rule out non-unique random number
# candidates in same row
#
# Easy-to-read key:
#
# TOP ROW SQUARES    = [0:3]
# MIDDLE ROW SQUARES = [3:6]
# BOTTOM ROW SQUARES = [6:9]
#
######################################################
def blacklist_rows(box, boxes, square, blacklist):
    if (box.num == 1):
        if (square < 3):
            blacklist.extend(boxes[0].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[0].squares[3:6])
        else:
            blacklist.extend(boxes[0].squares[6:9])
    elif (box.num == 2):
        if (square < 3):
            blacklist.extend(boxes[0].squares[0:3])
            blacklist.extend(boxes[1].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[0].squares[3:6])
            blacklist.extend(boxes[1].squares[3:6])
        else:
            blacklist.extend(boxes[0].squares[6:9])
            blacklist.extend(boxes[1].squares[6:9])
    elif (box.num == 4):
        if (square < 3):
            blacklist.extend(boxes[3].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[3].squares[3:6])
        else:
            blacklist.extend(boxes[3].squares[6:9])
    elif (box.num == 5):
        if (square < 3):
            blacklist.extend(boxes[3].squares[0:3])
            blacklist.extend(boxes[4].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[3].squares[3:6])
            blacklist.extend(boxes[4].squares[3:6])
        else:
            blacklist.extend(boxes[3].squares[6:9])
            blacklist.extend(boxes[4].squares[6:9])
    elif (box.num == 7):
        if (square < 3):
            blacklist.extend(boxes[6].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[6].squares[3:6])
        else:
            blacklist.extend(boxes[6].squares[6:9])
    elif (box.num == 8):
        if (square < 3):
            blacklist.extend(boxes[6].squares[0:3])
            blacklist.extend(boxes[7].squares[0:3])
        elif (square < 6):
            blacklist.extend(boxes[6].squares[3:6])
            blacklist.extend(boxes[7].squares[3:6])
        else:
            blacklist.extend(boxes[6].squares[6:9])
            blacklist.extend(boxes[7].squares[6:9])





######################################################
# Build blacklist to rule out non-unique random number
# candidates in same column
#
# Easy-to-read key:
#
# LEFT COLUMN SQUARES    = [0:7:3]
# MIDDLE COLUMN SQUARES  = [1:8:3]
# RIGHT COLUMN SQUARES   = [2:9:3]
#
#####################################################
def blacklist_cols(box, boxes, square, blacklist):
    if (box.num == 3):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[0].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[0].squares[1:8:3])
        else:
            blacklist.extend(boxes[0].squares[2:9:3])
    elif (box.num == 4):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[1].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[1].squares[1:8:3])
        else:
            blacklist.extend(boxes[1].squares[2:9:3])
    elif (box.num == 5):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[2].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[2].squares[1:8:3])
        else:
            blacklist.extend(boxes[2].squares[2:9:3])
    elif (box.num == 6):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[0].squares[0:7:3])
            blacklist.extend(boxes[3].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[0].squares[1:8:3])
            blacklist.extend(boxes[3].squares[1:8:3])
        else:
            blacklist.extend(boxes[0].squares[2:9:3])
            blacklist.extend(boxes[3].squares[2:9:3])
    elif (box.num == 7):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[1].squares[0:7:3])
            blacklist.extend(boxes[4].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[1].squares[1:8:3])
            blacklist.extend(boxes[4].squares[1:8:3])
        else:
            blacklist.extend(boxes[1].squares[2:9:3])
            blacklist.extend(boxes[4].squares[2:9:3])
    elif (box.num == 8):
        if (square == 0 or square == 3 or square == 6):
            blacklist.extend(boxes[2].squares[0:7:3])
            blacklist.extend(boxes[5].squares[0:7:3])
        elif (square == 1 or square == 4 or square == 7):
            blacklist.extend(boxes[2].squares[1:8:3])
            blacklist.extend(boxes[5].squares[1:8:3])
        else:
            blacklist.extend(boxes[2].squares[2:9:3])
            blacklist.extend(boxes[5].squares[2:9:3])



# Run the gauntlet!
def gen_num(box, boxes, blacklist):
    counter = 0
    while True:
        # Scrap generated squares for this box and try again (due to impossible logic situations)
        if (counter == 20):
            return False

        num = randint(1,9)

        # Verify num is not on blacklist and not already in the box where it is being generated
        if (num in blacklist):
            counter += 1
            continue
        elif (num in box.squares):
            counter += 1
            continue

        box.squares.append(num)
        return True



# Function to remove random numbers and make our completed grid an actual puzzle
def difficulty_mask(boxes, diff_rating):

    # Set number of squares to remove from generated puzzle
    if (diff_rating == 1):
        print("Difficulty: Easy")
        counter = randint(35,40)
    elif (diff_rating == 2):
        counter = randint(45,50)
        print("Difficulty: Moderate")
    else:
        counter = randint(55,60)
        print("Difficulty: Hard")

    # Create a copy of our boxes list so that the original can be printed out as the completed puzzle
    modified_boxes = deepcopy(boxes)
    box = 0
    square = 0

    while (counter > 0):

        # Randomize indeces since we need to know if a box has a value before blanking it
        # and cannot both check for a value and assign a blank in one line of code
        box = randint(0,8)
        square = randint(0,8)

        # If the box is an int, blank it out. Otherwise, skip it to preserve our counter
        if (type(modified_boxes[box].squares[square]) is int):
            modified_boxes[box].squares[square] = " "
            counter -= 1

    return modified_boxes


# Print one row of boxes at a time (left to right)
def print_box_row(left, middle, right):
    print()
    print(" ", left.squares[0], " | ", left.squares[1], " | ", left.squares[2], "    ", \
               middle.squares[0], " | ", middle.squares[1], " | ", middle.squares[2], "    ", \
               right.squares[0], " | ", right.squares[1], " | ", right.squares[2])
    print("-----|-----|-----  -----|-----|-----  -----|-----|-----")
    print(" ", left.squares[3], " | ", left.squares[4], " | ", left.squares[5], "    ", \
               middle.squares[3], " | ", middle.squares[4], " | ", middle.squares[5], "    ", \
               right.squares[3], " | ", right.squares[4], " | ", right.squares[5])
    print("-----|-----|-----  -----|-----|-----  -----|-----|-----")
    print(" ", left.squares[6], " | ", left.squares[7], " | ", left.squares[8], "    ", \
               middle.squares[6], " | ", middle.squares[7], " | ", middle.squares[8], "    ", \
               right.squares[6], " | ", right.squares[7], " | ", right.squares[8])
    print()



main()
