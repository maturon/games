# Sudoku-CLI

Generate a random sudoku puzzle. First choose a setting, then the puzzle is generated automatically. Afterward, press any key to see the answer.

## Usage

```bash
./sudoku.py
```

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)