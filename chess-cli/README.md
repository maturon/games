# Chess-CLI

chess-cli is a 2 player chess program played entirely in the command line! More features to be added later


## Usage

Requires chess notation knowledge in order to make moves. Basic syntax is:

- [SQUARE ROW] 			pawn move
- [SQUARE ROW]x[SQUARE] 	pawn capture
- [UNIT][SQUARE] 			minor/major piece move without capture
- [UNIT]x[SQUARE] 		capture a piece
- O-O 					castle kingside
- O-O-O 					castle queenside

Examples:

```
Nc4
Bxe3
Nxe3
d5
cxd5
```

## Pieces

- a-h		pawn (corresponding to row)
- N 		Knight
- B 		Bishop
- R 		Rook
- Q 		Queen
- K 		King

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)