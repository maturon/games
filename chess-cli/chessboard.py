import chess_pieces
import chess_logic



# Receive board container (list to contain objects), initialize with null values, then populate with chess piece objects
def init_squares(board):
    # Initialize squares with null values
    for x in range(64):
        board += [None]

    # Initialze pawns
    start_square = 8
    for x in range(8):
        pawn = chess_pieces.Pawn('White')
        board[start_square] = pawn
        start_square += 1
    start_square = 48
    for x in range(8):
        pawn = chess_pieces.Pawn('Black')
        board[start_square] = pawn
        start_square += 1

    # Initialize white pieces
    king      = chess_pieces.King('White')
    queen     = chess_pieces.Queen('White')
    rook      = chess_pieces.Rook('White')
    bishop    = chess_pieces.Bishop('White')
    knight    = chess_pieces.Knight('White')
    board[0]  = board[7] = rook
    board[1]  = board[6] = knight
    board[2]  = board[5] = bishop
    board[3]  = queen
    board[4]  = king

    # Initialize black pieces
    king      = chess_pieces.King('Black')
    queen     = chess_pieces.Queen('Black')
    rook      = chess_pieces.Rook('Black')
    bishop    = chess_pieces.Bishop('Black')
    knight    = chess_pieces.Knight('Black')
    board[56] = board[63] = rook
    board[57] = board[62] = knight
    board[58] = board[61] = bishop
    board[59] = queen
    board[60] = king



# Print a crude ascii board
def print_ascii_board(board):
    WHITE   = '\033[0m'
    counter = 63
    rank    = 8
    file    = 97
    print()
    print(WHITE + "  ┌─────────────────┐")
    for x in reversed(board):                               # Iterate through board backwards to print from top to bottom
        if (counter % 8 == 0):                              # For every 8 squares...
            counter2 = counter
            print(WHITE + str(rank) + WHITE + " │", end=' ')
            rank -= 1
            for y in range(8):                              # Print previous 8 squares but not in reverse order
                if (board[counter2] != None):
                    print(board[counter2].print_symbol + WHITE, end=' ')
                else:
                    print(' ', end=' ')                     # If square does not contain an object, print a blank
                counter2 += 1
            print("│")
        counter -= 1
    print(WHITE + "  └─────────────────┘")
    print("   ", end=' ')
    for x in range(1,9):
        print(chr(file), end=' ')
        file += 1
    print()
    print()



# Simple function that prints our game moves in standard chess notation
def print_game(moves):
    counter  = 0
    move_num = 1

    # Format and print move list as human-readable chess notation
    for x in moves:
        # Print game after black's move (every odd turn)
        if (counter % 2 != 0):
            print(str(str(move_num) + ". " + moves[counter - 1].ljust(7) + " " + moves[counter]))
            move_num += 1
        counter += 1



# Convert linear square numbers (0-63) to standard chess coords (letter + number)
def get_notation(num):
    # Determine which rank (horizontal row) the number falls under
    rank = int((num / 8) + 1)

    # Divide our num by 8 and convert the portion to the right of the decimal to a whole number, giving us our "leftover" num
    num /= 8
    num = (num - int(num)) * 1000

    # Default file starts at 'a', x will increment by 125 (the value right of the decimal of our initial number divided by 8 will be in an increment of 125) and the file letter increments with x until we have a match
    file = 'a'
    x = 0

    # Compare our "leftover" num against x in increments of 125
    for y in range(8):
        if (num == x):
            # Built-in func that allows us to 'iterate' through characters
            file = chr(ord(file) + y)
            break
        x += 125
    return [file, rank]



# Convert standard chess coords to a linear number scheme -> squares 0 (a1) through 63 (h8)
def get_coords(square):
    square = list(square)   # Convert our string into a list for easy processing
    # File determines vertical column and x starts at the ascii value of 'a' (97)
    file = square[0]
    x = 97

    for y in range(8):
        # Use built-in ord() to compare the numerical value of the file letter with its ascii equivalent, then subtract 97 to reduce it to a number between 0 and 7
        if (ord(file) == x):
            file = ord(file) - 97
            break
        x += 1

    # Calculate final number based on its file and how many ranks up the board it is (board[1])
    return ((int(square[1]) * 8) - (8 - file))




def get_square_color(square):
    # Get square colors based on square coords
    black_squares = []                              # Create list for all black squares
    white_squares = []                              # Create list for all white squares
    temp          = []                              # Create temporary container used to swap other 2 lists
    for x in range (0, 64):                         # Iterate through all 64 squares
        if (x % 2 == 0):                            # Store every odd square in black_squares list
            black_squares.append(x)
        else:
            white_squares.append(x)                 # Store every even square in white_squares list
        if ((x + 1) % 8 == 0):                      # Due to chessboard layout, after every 8 iterations, swap values between lists
            temp          = white_squares.copy()
            white_squares = black_squares.copy()
            black_squares = temp.copy()
            
    if (square in white_squares): return "White"    # Look for square arg in list and return appropriate square color
    else: return "Black"
